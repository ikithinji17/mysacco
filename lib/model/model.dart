import 'package:flutter/foundation.dart';

class User {
  final bool loggedIn;
  final String phone;

  User({@required this.phone, @required this.loggedIn});

  User copyWith({String token, bool loggedIn}) {
    return User(loggedIn: loggedIn ?? this.loggedIn, phone: phone?? this.phone);
  }

  User.fromJson(Map json) : loggedIn = json['loggedIn'],
        phone = json['phone'];

  Map toJson() => {
    'loggedIn': loggedIn,
    'phone': phone
  };
}

@immutable
class AppState {
  final User user;

  AppState({@required this.user});

  AppState.initialState() : user = User(loggedIn: false, phone: "");

  AppState.fromJson(Map json) : user =  User.fromJson(json['user']);

  Map toJson() => {'user': user};
}
