import 'package:flutter/material.dart';
import 'package:mysacco/Pages/Main/HomePage.dart';
import 'package:mysacco/Pages/Main/LoginPage.dart';
import 'package:mysacco/main.dart';

class LoginDecide extends StatefulWidget {

  final ViewModel model;

  LoginDecide(this.model);

  @override
  _LoginDecideState createState() => _LoginDecideState();
}

class _LoginDecideState extends State<LoginDecide> {

    @override
    Widget build(BuildContext context) {
      return new Scaffold(
          body: widget.model.user.loggedIn
              ?
          new HomePage(widget.model)
              :
          new LoginPage(widget.model)
      );
    }
}
