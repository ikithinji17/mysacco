import 'package:flutter/material.dart';
import 'package:mysacco/LoginDecide.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'redux/reducers.dart';
import 'redux/actions.dart';
import 'model/model.dart';
import 'redux/middleWare.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Store<AppState> store = Store<AppState>(appStateReducer,
        initialState: AppState.initialState(),
        middleware: [appStateMiddleware]);

    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primarySwatch: Colors.blue),
        home: StoreBuilder<AppState>(
          onInit: (store) => store.dispatch(GetUserAction()),
          builder: (BuildContext context, Store<AppState> store) =>
              MyHomePage(store),
        ),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final Store<AppState> store;

  MyHomePage(this.store);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: StoreConnector<AppState, ViewModel>(
        converter: (Store<AppState> store) => ViewModel.create(store),
        builder: (BuildContext context, ViewModel viewmodel) =>
            Container(child: LoginDecide(viewmodel)),
      ),
    );
  }
}

class ViewModel {
  final User user;
  final Function(User) onloginUser;
  final Function() onlogoutUser;

  ViewModel({this.user, this.onloginUser, this.onlogoutUser});

  factory ViewModel.create(Store<AppState> store) {
    _loginUser(User user) {
      print("Hello viewmodel");
      print(user.phone);
      store.dispatch(LoginUserAction(user));
    }

    _logoutUser() {
      store.dispatch(LogoutUserAction);
    }

    return ViewModel(
        user: store.state.user,
        onloginUser: _loginUser,
        onlogoutUser: _logoutUser);
  }
}