import 'package:flutter/material.dart';

class HomeMenuModel {
  final AssetImage assetImage;
  final String option;

  HomeMenuModel({this.assetImage, this.option});
}

List<HomeMenuModel> homeMenu = [
  new HomeMenuModel(
      assetImage: new AssetImage('images/withdraw_money_cl.png'),
      option: "Withdraw Money"),
  new HomeMenuModel(
    option: "Deposit Money",
    assetImage: AssetImage('images/withdraw_money_cl.png')
  ),
  new HomeMenuModel(
    option: "Buy Airtime",
    assetImage: AssetImage('images/buy_airtime_cl.png')
  ),
  new HomeMenuModel(
    option: "Account Balance",
    assetImage: AssetImage('images/account_balance_cl.png')
  ),
  new HomeMenuModel(
    option: "Account statement",
    assetImage: AssetImage('images/account_statement_cl.png')
  ),
  new HomeMenuModel(
    option: "Pay Loan",
    assetImage: AssetImage('images/loan_statement_cl.png')
  ),
  new HomeMenuModel(
    option: "Loan Statement",
    assetImage: AssetImage('images/loan_statement_cl.png')
  )
];
