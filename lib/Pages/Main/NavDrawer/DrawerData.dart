class draweroption {
  final String name;

  draweroption({this.name});
}

class DrawerOptionsModel {
  final String head;
  final bool hasOptions;
  final List<draweroption> children;

  DrawerOptionsModel({this.head, this.hasOptions, this.children});
}


List<DrawerOptionsModel> options = [
  new DrawerOptionsModel(head: "Home", hasOptions: false, children: []),
  new DrawerOptionsModel(head: "Transact", hasOptions: true, children: [
    new draweroption(name: "Withdraw Money"),
    new draweroption(name: "Deposit Money"),
    new draweroption(name: "Buy Airtime"),
    new draweroption(name: "Pay Loan"),
    new draweroption(name: "Pay Bill"),
    new draweroption(name: "Inter-account Transfer")
  ]),
  new DrawerOptionsModel(head: "My Account", hasOptions: true, children: [
    new draweroption(name: "Account Balance"),
    new draweroption(name: "Account Statement"),
    new draweroption(name: "Change Password"),
  ]),
  new DrawerOptionsModel(head: "Loans", hasOptions: true, children: [
    new draweroption(name: "Pay Loans"),
    new draweroption(name: "Apply Loans"),
    new draweroption(name: "Loan Approval"),
    new draweroption(name: "Loan Balance"),
    new draweroption(name: "Loan Statement"),
    new draweroption(name: "My Loan Guarantors"),
    new draweroption(name: "Loan Guaranteed"),
  ]),
  new DrawerOptionsModel(head: "Other Services", hasOptions: true, children: [
    new draweroption(name: "Request Information"),
    new draweroption(name: "Chequebook request"),
    new draweroption(name: "Stop Cheque"),
    new draweroption(name: "Stop ATM Card"),
  ]),
  new DrawerOptionsModel(head: "Messages", hasOptions: false, children: []),
  new DrawerOptionsModel(head: "Contact Us", hasOptions: false, children: []),
  new DrawerOptionsModel(head: "Logout", hasOptions: false, children: []),
];