import 'package:flutter/material.dart';
import 'package:mysacco/main.dart';

import 'DrawerData.dart';

class MyDrawer extends StatelessWidget {
  final ViewModel model;

  MyDrawer(this.model);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Drawer(
        child: ListView(
      children: <Widget>[
        new UserAccountsDrawerHeader(
          accountName: Text(
            model.user.phone,
            style: TextStyle(fontSize: 13),
          ),
          accountEmail: Text("NACICO SACCO SOCIETY LTD",
              style: TextStyle(
                fontSize: 12,
              )),
          currentAccountPicture: new CircleAvatar(
            child: Icon(Icons.person),
          ),
        ),
          ListTile(
            title: Text(options[0].head),
          ),
          ExpansionTile(
            title: Text(options[1].head),
            children: <Widget>[
              ListTile(title: Text(options[1].children[0].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
              ListTile(title: Text(options[1].children[1].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
              ListTile(title: Text(options[1].children[2].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
              ListTile(title: Text(options[1].children[3].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
              ListTile(title: Text(options[1].children[4].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
              ListTile(title: Text(options[1].children[5].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ],
          ),
        ExpansionTile(
          title: Text(options[2].head),
          children: <Widget>[
            ListTile(title: Text(options[2].children[0].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ListTile(title: Text(options[2].children[1].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ListTile(title: Text(options[2].children[2].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
          ],
        ),
        ExpansionTile(
          title: Text(options[3].head),
          children: <Widget>[
            ListTile(title: Text(options[3].children[0].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ListTile(title: Text(options[3].children[1].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ListTile(title: Text(options[3].children[2].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ListTile(title: Text(options[3].children[3].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ListTile(title: Text(options[3].children[4].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ListTile(title: Text(options[3].children[5].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ListTile(title: Text(options[3].children[6].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
          ],
        ),
        ExpansionTile(
          title: Text(options[4].head),
          children: <Widget>[
            ListTile(title: Text(options[4].children[0].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),), ),
            ListTile(title: Text(options[4].children[1].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ListTile(title: Text(options[4].children[2].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
            ListTile(title: Text(options[4].children[3].name, style: TextStyle(color: Colors.grey, fontSize: 13.0),),),
          ],
        ),
          ListTile(
            title: Text(options[5].head),
          ),
          ListTile(
            title: Text(options[6].head),
          ),
          ListTile(
            title: Text(options[7].head),
          ),
      ],
    ));
  }
}
