import 'package:flutter/material.dart';

import 'package:mysacco/main.dart';
import 'package:mysacco/model/model.dart';

import 'package:mysacco/Pages/Blocs/loginBloc.dart';

class LoginPage extends StatefulWidget {
  final ViewModel model;

  LoginPage(this.model);

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final loginbloc = new LoginBloc();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("NACICO SACCO Mobile"),
      ),
      body: new SingleChildScrollView(
          child: new Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.all(16.0),
        child: new Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            SizedBox(
              height: 10.0,
            ),
            Text(
              "LOGIN  ",
              style: TextStyle(fontSize: 18.0,),
            ),
            Icon(
              Icons.account_balance,
              size: 88.0,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text("NACICO SACCO"),
            SizedBox(
              height: 20.0,
            ),
            StreamBuilder<String>(
              stream: loginbloc.phone,
              builder: (context, snapshot) => TextField(
                    onChanged: loginbloc.phoneChanged,
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: "Enter Phone Number",
                        errorText: snapshot.error,
                        labelText: "Phone Number"),
                  ),
            ),
            SizedBox(
              height: 20.0,
            ),
            StreamBuilder<String>(
              stream: loginbloc.password,
              builder: (context, snapshot) => TextField(
                    onChanged: loginbloc.passwordChanged,
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: "Enter Your Password",
                        errorText: snapshot.error,
                        labelText: "Password"),
                  ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Align(
              alignment: FractionalOffset.bottomRight,
              child: StreamBuilder<bool>(
                stream: loginbloc.submitCheck,
                builder: (context, snapshot) => RaisedButton(
                      color: Colors.blue,
                      onPressed:
                          snapshot.hasData ? () => callReducer(context) : null,
                      child: Text("Login"),
                    ),
              ),
            )
          ],
        ),
      )),
    );
  }

  callReducer(BuildContext context) {
    print("Here Logging in");
    String phone;
    loginbloc.phone.listen((data){
      print(" Your Phone Number is $data");
      phone = data;
      print("A Phone $phone");
      final snackbar  = SnackBar(content: Text("Login In $data"));

      Scaffold.of(context).showSnackBar(snackbar);
      widget.model
          .onloginUser(User(loggedIn: true, phone: "$data"));
    }, onDone: () {
      print("Done");

    }, onError:(error) {
      print(error);
    },
    );
  }
}
