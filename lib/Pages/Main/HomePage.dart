import 'package:flutter/material.dart';
import 'package:mysacco/main.dart';
import 'package:mysacco/Pages/Main/NavDrawer/DrawerData.dart';
import 'NavDrawer/MyDrawer.dart';
import 'WithDrawMoney.dart';

class HomePage extends StatefulWidget {
  final ViewModel model;

  HomePage(this.model);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget Showoptions(BuildContext context) {
    options.forEach((action) {
      return ListTile(
        title: Text(action.head),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("NACICO SACCO Mobile"),
      ),
      drawer: MyDrawer(widget.model),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: new Column(
          children: <Widget>[
            new Card(
              child: new Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: new Image(
                      image: AssetImage('images/mysacco_logo.png'),
                      height: 80.0,
                    ),
                  ),
                  new Divider(
                    color: Colors.black45,
                  ),
                  new Text("SACCO SLOGAN")
                ],
              ),
            ),
            new SizedBox(
              height: 1.0,
            ),
            new GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(
                        builder: (context) => new Withdraw(widget.model))
                );
              },
              child: new Card(
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: new Row(
                    children: <Widget>[
                      new Image(
                        image: AssetImage('images/withdraw_money_cl.png'),
                        height: 32.0,
                        width: 32.0,
                      ),
                      new SizedBox(
                        width: 16.0,
                      ),
                      new Text("Withdraw Funds")
                    ],
                  ),
                ),
              ),
            ),
            new SizedBox(
              height: 1.0,
            ),
            new Card(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Row(
                  children: <Widget>[
                    new Image(
                      image: AssetImage('images/withdraw_money_cl.png'),
                      height: 32.0,
                      width: 32.0,
                    ),
                    new SizedBox(
                      width: 16.0,
                    ),
                    new Text("Deposit Money")
                  ],
                ),
              ),
            ),
            new SizedBox(
              height: 1.0,
            ),
            new Card(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Row(
                  children: <Widget>[
                    new Image(
                      image: AssetImage('images/buy_airtime_cl.png'),
                      height: 32.0,
                      width: 32.0,
                    ),
                    new SizedBox(
                      width: 16.0,
                    ),
                    new Text("Buy Airtime")
                  ],
                ),
              ),
            ),
            new SizedBox(
              height: 1.0,
            ),
            new Card(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Row(
                  children: <Widget>[
                    new Image(
                      image: AssetImage('images/account_balance_cl.png'),
                      height: 32.0,
                      width: 32.0,
                    ),
                    new SizedBox(
                      width: 16.0,
                    ),
                    new Text("Account Balance")
                  ],
                ),
              ),
            ),
            new SizedBox(
              height: 1.0,
            ),
            new Card(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Row(
                  children: <Widget>[
                    new Image(
                      image: AssetImage('images/account_statement_cl.png'),
                      height: 32.0,
                      width: 32.0,
                    ),
                    new SizedBox(
                      width: 16.0,
                    ),
                    new Text("Account Statement")
                  ],
                ),
              ),
            ),
            new SizedBox(
              height: .0,
            ),
            new Card(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Row(
                  children: <Widget>[
                    new Image(
                      image: AssetImage('images/loan_statement_cl.png'),
                      height: 32.0,
                      width: 32.0,
                    ),
                    new SizedBox(
                      width: 16.0,
                    ),
                    new Text("Pay Loans")
                  ],
                ),
              ),
            ),
            new SizedBox(
              height: 1.0,
            ),
            new Card(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Row(
                  children: <Widget>[
                    new Image(
                      image: AssetImage('images/loan_statement_cl.png'),
                      height: 32.0,
                      width: 32.0,
                    ),
                    new SizedBox(
                      width: 16.0,
                    ),
                    new Text("Loan Statement")
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: new FloatingActionButton.extended(
        onPressed: null,
        label: new Text("Messages"),
        icon: new Icon(Icons.message),
      ),
    );
  }
}
