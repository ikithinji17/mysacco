import 'package:flutter/material.dart';
import 'package:mysacco/Pages/Blocs/allBlocs.dart';

import '../../main.dart';
import 'NavDrawer/MyDrawer.dart';
import '../Blocs/validator.dart';

class Withdraw extends StatefulWidget {
  final ViewModel model;

  Withdraw(this.model);

  @override
  _WithDrawPageState createState() => _WithDrawPageState();
}

class _WithDrawPageState extends State<Withdraw> {
  static String _currentAccountSelected = 'Select Account';
  static String _currentServiceSelected = 'Select Service';
  List<String> accounts = [_currentAccountSelected, "Acc: 0532013001", "Acc: 0532013002"];
  List<String> Servicees = [_currentServiceSelected, "Safaricom Mpesa", "Airtel Money", "Equitel Money"];
  final allblocs = new AllBlocs();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("Transact"),
        actions: <Widget>[],
      ),
      drawer: new MyDrawer(widget.model),
      body: new Padding(
        padding: const EdgeInsets.all(20.0),
        child: new Column(
          children: <Widget>[
            new Align(
              alignment: Alignment.centerLeft,
              child: new Text(
                "Withdraw Money",
                style: TextStyle(fontSize: 21.0, ),
              ),
            ),
            new Divider(),
            SizedBox(
              height: 10.0,
            ),
            new DropdownButton<String>(
              isExpanded: true,
              elevation: 5,
              items: accounts.map((String item) {
                return new DropdownMenuItem(
                  child: Text(item),
                  value: item,
                );
              }).toList(),
              onChanged: (String newValueSelected) {
                setState(() {
                  _currentAccountSelected = newValueSelected;
                });
              },
              value: _currentAccountSelected,
            ),
            SizedBox(
              height: 10.0,
            ),
            new DropdownButton<String>(
              isExpanded: true,
              elevation: 5,
              items: Servicees.map((String item) {
                return new DropdownMenuItem(
                  child: Text(item),
                  value: item,
                );
              }).toList(),
              onChanged: (String newValueSelected) {
                setState(() {
                  _currentServiceSelected = newValueSelected;
                });
              },
              value: _currentServiceSelected,
            ),
            SizedBox(
              height: 10.0,
            ),
            StreamBuilder<String>(
              stream: allblocs.amount,
              builder: (context, snapshot) => TextField(
                onChanged: allblocs.amountChanged,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Enter Withdraw Amount",
                    errorText: snapshot.error,
                    labelText: "Amount"),
              ),
            ),
            new Column(
              children: <Widget>[
                new Radio(value: 0, groupValue: "0", onChanged: null)
              ],
            )
          ],
        ),
      ),
    );
  }
}
