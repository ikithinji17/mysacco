import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:mysacco/Pages/Blocs/validator.dart';

class AllBlocs extends Object with Validators implements BaseBloc {
  final _amountController = BehaviorSubject<String>();

  Function(String) get amountChanged => _amountController.sink.add;

  Stream<String> get amount =>
      _amountController.stream.transform(withdrawAmtValidator);

  @override
  void dispose() {
    _amountController?.close();
  }
}


abstract class BaseBloc {
  void dispose();
}