import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:mysacco/Pages/Blocs/validator.dart';

class LoginBloc extends Object with Validators implements BaseBloc {
  final _phoneController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();

  Function(String) get phoneChanged => _phoneController.sink.add;
  Function(String) get passwordChanged => _passwordController.sink.add;


  Stream<String> get phone => _phoneController.stream.transform(phoneValidator);

  Stream<String> get password =>
      _passwordController.stream.transform(passwordValidator);

  Stream<bool> get submitCheck =>
      Observable.combineLatest2(phone, password, (ph, psw) => true);

  @override
  void dispose() {
    _phoneController?.close();
    _passwordController?.close();
  }
}

abstract class BaseBloc {
  void dispose();
}
