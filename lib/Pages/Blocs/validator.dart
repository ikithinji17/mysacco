import 'dart:async';

mixin Validators{
  var phoneValidator = StreamTransformer<String,String>.fromHandlers(
    handleData: (phone, sink){
      if(phone.length > 9){
        sink.add(phone);
      }else{
        sink.addError("Phone Number Length is invalid");

      }
    }
  );

  var passwordValidator = StreamTransformer<String,String>.fromHandlers(
      handleData: (password, sink){
        if(password.length >= 4){
          sink.add(password);
        }else{
          sink.addError("Password is too short");

        }
      }
  );

  var withdrawAmtValidator = StreamTransformer<String, String>.fromHandlers(
    handleData: (amount, sink){
     if(amount.length != 0){
       sink.add(amount);
     }else{
       sink.addError("Please enter Amount");
     }
    }
  );
}