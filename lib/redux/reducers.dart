import 'package:mysacco/model/model.dart';
import 'actions.dart';


AppState appStateReducer(AppState state, action){
  return AppState(
      user: userReducer(state.user, action)
  );
}


User userReducer(User state, action){
  if (action is LoginUserAction ){
    print(action.user.phone);
    return User(loggedIn: action.user.loggedIn, phone: action.user.phone);
  }

  if (action is LogoutUserAction){
    return User(loggedIn: false, phone: null);
  }

  if (action is LoadedUserAction){
    return action.user;
  }
  return state;
}
