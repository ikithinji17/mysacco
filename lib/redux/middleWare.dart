import 'dart:async';
import 'dart:convert';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'actions.dart';
import 'package:mysacco/model/model.dart';


void saveToPref(AppState state) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var uString = json.encode(state.toJson());
  print("running appstate");
  print(uString);
  await preferences.setString('userState', uString);
}

Future<AppState> loadFromPrefs() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var uString = preferences.getString("userState");

  if (uString != null) {
    Map map = json.decode(uString);
    return AppState.fromJson(map);
  }

  return AppState.initialState();
}

void appStateMiddleware(Store<AppState> store, action,
    NextDispatcher next) async {
  next(action);

  if (action is LoginUserAction ||
      action is LogoutUserAction) {
    saveToPref(store.state);
  }
  if (action is GetUserAction) {
    await loadFromPrefs().then((state) =>
        store.dispatch(LoadedUserAction(state.user)));
  }
}