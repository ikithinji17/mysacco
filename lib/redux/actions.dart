import 'package:mysacco/model/model.dart';

class LoginUserAction {
  final User user;

  LoginUserAction(this.user);
}

class LogoutUserAction {}

class GetUserAction {}

class LoadedUserAction {
  final User user;

  LoadedUserAction(this.user);
}
