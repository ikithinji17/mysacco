import 'package:flutter/material.dart';

Widget allFields() {
  return Container(
      margin: new EdgeInsets.all(15.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new TextFormField(
            validator: (value) => validator(value, "phone number"),
            keyboardType: TextInputType.number,
            autofocus: true,
            decoration:
            new InputDecoration(labelText: "Phone Number"),
          ),
          new TextFormField(
            validator: (value) => validator(value, "password"),
            obscureText: true,
            decoration: new InputDecoration(
              labelText: "Password",
            ),
          ),
        ],
      ),
  );
}

validator(String value, String place) {
  if (value.isEmpty) {
    return "please enter your "+ place;
  }
}